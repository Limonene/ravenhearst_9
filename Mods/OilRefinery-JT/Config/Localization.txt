Key,Source,Context,English
oilRefineryRH1,blocks,Container,Oil Rig (Empty)
oilRefineryRH2,blocks,Container,Oil Rig (Finding Nodes)
oilRefineryRH3,blocks,Container,Oil Rig (Starting Drill)
oilRefineryRH4,blocks,Container,Oil Rig (25% Full)
oilRefineryRH5,blocks,Container,Oil Rig (50% Full)
oilRefineryRH6,blocks,Container,Oil Rig (75% Full)
oilRefineryRH1Schematic,blocks,Container,Oil Rig Recipe

oilRefineryRH1Desc,blocks,Container,Place this oil rig on wide open terrain. After placing it will go through 6 phases at it looks for underground oil and drills for it. When the drill is done you will be able to loot it and it will begin drilling again.
oilRefineryRH1Full,blocks,Container,Oil Rig Full
oilRefineryRH1FullDesc,blocks,Container,Oil Rig has found all the oil it can find.
