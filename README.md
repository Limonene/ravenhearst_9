.. ***Jax Teller*** *and* ***The Ravenhearst Team present*** ...

# 🧟‍♀️ ***Ravenhearst 9.1.2b5*** 🧟
---

* Website: http://ravenhearst.enjin.com/
* GitLab: [@JaxTeller718](https://gitlab.com/JaxTeller718)
* Twitter: <a href="https://twitter.com/JaxTeller718">@JaxTeller718</a>

## Show your support

<a href="https://www.patreon.com/ravenhearstmod">
  <img src="https://i.imgur.com/zc9P26t.png" width="160">
</a>

<a href="https://www.patreon.com/ravenhearstmod">
  <img src="https://c5.patreon.com/external/logo/become_a_patron_button@2x.png" width="160">
</a>

## Scroll Below for Latest Patch Notes

### Features & Content :zap:
Welcome to Ravenhearst a place where your nightmares grow and fester and the world around you is trying to kill you at every turn. Ravenhearst is a full overhaul mod that uses the amazing 7 Days to Die and turns expectations in its head. Every aspect of the vanilla game has been tweaked or tuned to increase challenge and difficulty. Ravenhearst has often been called the mod that is not for everyone. It is best played by those that have a basic understanding of the vanilla 7 Days to Die experience, and for those looking to challenge themselves.

All the Worlds a Stage
The ENTIRE game is now gamestaged. Until gamestage 5, you will see slightly weaker versions of zombies in the world, helping to bridge the gap into Ravenhearst. The incredibly talented @Yakov and @Redbeardt  has enabled me the chance to gamestage every aspect of the game. Wilderness Spawns, Wandering Hordes, Biome Spawns, Night Time Spawns ALL of it is now set according to your gamestage. Night Terrors do not spawn pre Gamestage 50. Gamestage tiers are as follows:

GS1 - Normal Zeds
GS100 - Tier 2
GS500 - Tier 3
GS700 - Tier 4
This ensures you do not get overwhelmed and also that the challenge increases as your level increases
 

The Night Bleeds
The nights in Ravenhearst are full of disgusting creatures and terrifying sounds. Night Walkers are zombies who seek blood and are faster than a normal zombie. But fear not they disappear by morning... until you grow stronger. As your gamestage increases so do the chance that they will remain in the world!

Yakov and Redbeard's Suite of DLL Harmony Tweaks
It goes without saying that Redbeard and Yakov has been a major driving force behind Ravenhearst. They have designed a host of dll tweaks to game mechanics to increase difficulty and functionality. Here is a sampling of what They have done:
 
• Anti Nerd Poling - No longer can you jump and place frames. You have to plan out your builds with ladders and other blocks now. More realistic and challenges people to come up with new ways of overcoming challenges

• Recipe Ingredient Limit Removal - Recipes can now have dozens of ingredients so we are no longer restricted to 5. If a recipe has MORE than 5 there will be an arrow below the description you can click. It will scroll through the pages of ingredients.

• Blood Moon Vehicle Disabled - Our mod introduces challenges we expect players to face. Vehicles will become disabled on Horde Night. Bicycles will still work. Think of red moon and horde night like an EMP bomb, electricity is whacked during it.

• Recipe Count Memory - A configurable option in our menu allows you to remember the last amount of a crafted recipe quantity. So if you made 100 wood, the next time you open it it will read 100. This can be turned off via options..

• Skill Points Bonus - You will now gain 2 Skill Points every 10 levels as a boost to your perks. You can use these points just like any other and they are automatically added. So every 10 levels you have something to look forward to!
• Gamma Limiter - This may cause some controversy but we have set a limit on Gamma to 75. The reason for this is we have noticed players jacking their gamma up so they can see in house and at night. Some of these pois were designed with shadowy dark corners in mind. A little gamma bump is ok but bumping to 100 defeats the work put in by our POI makers. This is till open to some editing and tuning.

• Trader XP Removal - You will no longer level your Player by selling items. This is a gameplay decision made by me. I want actions to count for your level not selling to traders. Other aspects of traders are the same... for now

• Shared XP Limiter - When shared party XP first released we all assumed that it would be for questing and groups who work together. Recently it is accounting for people who do not level their weapons or skills but gain levels from party XP. This has led to complaints of tough zombies and unfair gamestages. We have hardcoded the xp sharing to 200M. The reason is gamestage related. Too many undergeared, under powered players who don't go questing with their partners are getting killed by zombies at the gamestage when their skills are not ready.

• Modified Descriptions - Now the station something is crafted in and the tool you need are automatically displayed at the end of every description. Descriptions will also display the number of slots an item has.

• Airdrop Spawning - Zombies will now spawn dependent on gamestage at airdrops. You will have to fight your way to the airdrop through a horde of undead.
• The Final Stand - After you finish your Class and Journal Quests, you will start a long final stand against the Undead. This includes special spawns, Blood Moons and more surprises. Do not start this undergeared!

• Quest Crafting Warning - A message will display if you leave a table while crafting an item needed to complete a quest.

• Reverse Rotation - QoL addition that allows you to reverse a rotation on blocks placed using LeftShift and LeftMouseClick.

• Stash Backpack - The original stash backpack you know and love designed. Click "X" to lock any slot you want in your backpack and move same items to boxes with a simple click. These options save on exit unlike Vanilla.

• Vehicle Pickup Disable - Realistic weights on vehicles. You can no longer pick up and store your heavy vehicles. Bicycles can be picked up. Be careful while driving and prepare to dig a vehicle out if it is stuck.


All New Game Options

You can set quest fetches to show indicators on the map, enable headshots only mode, control the size and frequency of wandering hordes, increase range zombies can see you at and enable or disable rage. These settings work on servers as well and a config.yaml file is included in the Mods/0-RHCore Folder.

 

• City Zombie Multiplier - Zombies will now spawn around groups of pois, making cities dangerous. You can control the multiplier in settings
• New Video Options - Field of view setting, show crosshairs, change the color of crosshairs or completely disable activation texts for a true clear Hud

• Enable/Disable Custom Font - Redbeard has selected a gorgeous font for our mod but we understand some may find it hard to read so you can no enable and disable it in options.

• PermaDeath Mode - You can now enable and disable a perma death mode from the RH menu. This adds a permanent death using Wellness.
•Wellness - Wellness makes it's return to 7 Days. Eat the right foods and water to increase your stats. Dying will reduce your Maxes down to 50. Build it back up by consuming better food and drink.

•Active Ingredients - You can now click an ingredient in the recipe list to link you to how to make that ingredient. He has also created a fantastic transparent paper doll that pops up to let you know you have armor that needs to be repaired (below 50%).

•Crop Damage - Zombies will now trample your crops and plants. So make sure you protect them at all costs.

•Sphereiicore is also used as is his Take and Replace and disable trader protection. That's right, traders are always open but no longer are invulnerable!


Living Off The Land
Farming has received an overhaul. Farm plots are now an end game goal and allow you to grow underground. Water is required for the plots to grow. The Farm Table is where you can craft seeds from crops or from seed packs you find out in the world. 

We have added back in Fertilizer, the Composter and yes Turds. Fertilizer made from the compost can be used on existing crops to increase yield. When destroyed, the crop returns to an unfertilized state, to be fertilized again.

Ground farming with a hoe has been brought back! Water will now deplete as your crops grow. You can work towards an irrigation pipe system. Irrigation will prevent water from being depleted.

Multiple new crops have been added like wheat, tomatoes, lettuce and more and these crops have been integrated into recipes new and old.

Infection Matters
When you are infected it will now kill you within 3 days. You MUST find a cure for your infection. It will persist through death. If you do not find a cure it will kill you. Chewing glass no longer represents a cure. Beehives can be made once the schematic is found. Tree stumps in the world have a chance to drop honey combs which can be crafted into honey to lower infections. This zombie apocalypse is very real and very dangerous.


Too Much Too Soon
Loot has undergone a complete rebalance (and may again in future versions) so that you are not swimming in loot early game. Scavenging and exploring now matters as you hunt for the best weapons and tools. Loot lists have been readjusted so they make more sense to what you would find in them.


From My Cold Dead Hands
In the ongoing quest to preserve what we love about 7 Days, we have readded some pulled game mechanics. Parts for guns are as they were with each firearm having specific parts you need to find to assemble a weapon like frames, handles and cylinders. More returns to former mechanics loved by many is ongoing for future updates.

Progression that means Progressing
Levelling has been slowed down quite a bit to give the old way of doing things more prominence. You need to get out there and work those quests and use those weapons. All main attributes are now Player Level dependent. Perks have been rebalanced so as not to give the appearance of a character becoming Superman by Perk 5 of everything.

Progression becomes way more exciting with the addition of unlockable recipes for every Action Skill. Every 20 levels of a skill will offer an unlock of a tool and weapon to that specific skill. So as you level and increase your Action Skills you will learn more recipes from the skill you are perfecting. 


Actions Speak Louder than Perks
Action Skills have returned. You will receive a slight bonus in stats every time you level an Action Skill. This is Learn by Doing. Use axes and picks to level Construction, Bows to level Archery, Blades to level.... well Blades. You can keep track of your Action Skills progress on the skills screen.


Quality Control
Quality has been returned to 7 Days to Die. Each tool and weapon now has a quality level. 1-1000. 

Each 20th level is considered next tier and every single item has had stats attached to them so that finding new weapons and tools is no longer a boring process. Loot is exciting again! Find level 72 pickaxe, but know there are better out there. This will promote leveling as well as scavenging. 

But beware. You will need to increase your main Construction Skill because every repair will cost you 100 Quality Points. Every 20 levels of Construction Skill will drop that by 20 points. So Construction Level 20 will drop the penalty to 80 QP. Const Level 40 will drop it to 60 QP Loss and so on. Save old or found tools to reduce this quality hit early on using the combine feature in the workbench mentioned above.

Better Watch That Weapon Boy
Along with Quality returning we have Degradation returning as well! Your weapon and tool will now degrade by 100 Quality Levels each time you repair it at the start.

On top of this we've taken care of any mods that are in your item or weapon. If when repairing a weapon the quality reduction results in a lose of a mod slot any mod in that lost slot will be placed in the players inventory. Be careful to have a spare slot though as a full bag and inventory will result in the mod falling on the floor!

So how do you salvage this? By leveling Construction Tools. Each 20th level of Construction Tools after level 20 reduces that number by 20. So level 21 in Const Tools you will lose 80 Durability and so on. A true progression system.


The Home You Have Always Dreamed Of
Ravenhearst likes to pride itself on being a builders dream mod. We have added dozens of new deco items for you to craft and decorate with, a number of new player storage options, movie posters you can collect, art you can collect, TV's, holiday items, playable radios and record players as well as made previously unavailable vanilla deco items accessible for crafting in our Decor Table. We are constantly adding new items as well.

Apart from over 50 new deco blocks i have also created several new block placeholders for poi makers. From new corpses to arcade and slot machines to miscellaneous trash decos like liquor bottles and posters and potted plants, new mail box and trash can placeholders, now POI builders who wish to make RH specific pois can design them with variety in mind.

Film Buff
You will find many collectible posters in the world, ranging from movies to TV shows to art pieces. The selection is ever changing ad growing and fit perfectly in the basement of your shelter!
 

Backpacks
The backpack perk is now gone.
In its place are backpacks that you can find in the world that unlocks three slots at a time to your inventory. These can be found in dressers, semis and other clothing related loot spots.

The Journal
We have introduced a quest based player journal that will take you on a questline all while teaching you the basic added mechanics of Ravenhearst. If you complete all the chapters of this journal you will receive exclusive recipes to craft some pretty cool items. The journal is not meant to be rushed through, rather it is meant to compliment your journey so do not worry too much about finishing it immediately.


Starvation
Food and water have undergone a major rework. Food values have been rebalanced, new food added, animals rebalanced for their meat yields and now they give red or white meat. Water is also now contaminated and you will need to build a water filtration unit to decontaminate it before you can drink it safely.

POI Wonderland

We have added many new POIs from around the 7 Days Community to fill our world with unique buildings. From zoos to parks to airports and police stations there is always something new to find in our world.!


You Have Some Red On Ya
Zombies come in many forms. We have added additional looks to zombies. Higher tiered zombies will sound and look different so you can tell them apart. Night Terrors make their return as well, with favorites such as The Butcher, The Prisoner and the Mechanic haunting the nights and preparing to drink your blood. Terrors Have Been Removed for Now But Will Return In a Future Update

Complex Crafting
Ravenhearst has always been about more realistic crafting and this version is no exception. At start you are given a Personal Workbench. This is your hub for all things crafting. The backpack has very limited recipes, just the absolute basics. You will need to carry this table with you. It can be picked up easily by destroying it. 

We have added workbenches and stations such as the Farm Table, the Armor Bench, The Ammunition Table, The Water Filtration Unit, Survival Campfire, Mechanic Bench, Research Table and the Decor Table, all with its own uses and recipes. This will help you find what you are looking for better and also spice up that basement work area.

Along with the stations, there are complex recipes such as tool heads for crafting tools, molds for forge crafting and vehicle parts. Vanilla recipes have all been changed to reflect new materials such as screws, tarps, twine and recipes like concrete and cobblestone have been made more complex.


Vehicular Homicide
We have reworked how vehicles are made completely. You will need to find engine parts and more from harvesting vehicles with the crowbar. There is now an Mechanic Bench and those are used to construct parts. You will have to build chassis from scratch using parts like car seats, alternators, sparkplugs and more.

We have also added over a dozen new vehicles for you to collect such as community favorites from bdubya like the Box Truck and Jeep! All of these vehicles have their own unique assemblies and parts.

Barnes and Noble? Where!
All schematics now scrap into Knowledge Points, which can be used with Ink found in the world in the Research Desk to craft any missing recipes and perk books. 

There is also a set of limited edition novels you can find in the world made by sinder that scraps into some paper for you to use. True collectors may find value in finding all of the novels left in the world. Some book stores or Research Facilities may have remnants of working Research Desks so make sure to search all of them.


Radiation
You will notice green smoke filled areas on your journey. This represents a Tier 5 POI that is radiated.

There is no way to explore these without a hazmat suit, which can then be upgraded to an Wasteland hazmat suit that will allow you to explore the Wastelands! Inside you will find military crates and chests that house end game weapons, tools, schematics and resources that can not be found in normal areas. Do not tackle these pois too early though. They are extremely dangerous. 


### Patch Notes for 9.1.2b5 👾

- Reduced Animal Spawns Overall
- Updated Scrapping Items Tip on Loading Screen
- Allowed the Excavator to Be Moved By Taking it
- Increased Economic Value on Novels and Comics
- Door Knobs Can Now Be Crafted in Blacksmith Forge
- Unlock Info and Naming Fixed on Painted Storage Boxes
- Fixed Repair Kits Being Made in Tanning Racks
- Added Smoker Schematic to Chef Class with Special Chef Only Recipes
- Fixed All Secondary Class Reward Texts
- Reduced Food Values on Fried Bacon
- Increased Harvest Counts on Punching Grass


### Patch Notes for 9.1.2b4 👾

- While a wipe is not REQUIRED it is advised to do so if you can. There are multiple fixes for POIs, New POIs, Fixes for Broken Stations in POIs and Perk Fixes. 
- It is strongly advised you delete your Mods folder and download this new one into your folder instead of just replacing. Files have been deleted.


- Updated Read Me
- Tier 2 Quests Now Give a Chance at Forged Iron as Reward
- Forged Iron Quest Reward Lowered to 25
- Fixed Splash Screen Not Fading Away (*Redbeard*)
- Added "Force High Res Textures" Option in Video Options to Fix Blurry Textures Issue (*Redbeard*)
- Cleaned Up 0-RHCore Folder (*Redbeard*)
- Fixed Font Option Not Saving (*Redbeard*)
- Cardboard Storage Box Upgraded to 28 Slots (Please advise your Cardboard Storage MAY Disappear due to this change. Empty it before updating)
- Increased Durability on All Knuckle Weapons
- Increased stack Sizes of Empty Buckets to 10
- Increased Stack Size of Full Buckets to 5
- Added Hand Models for Some Foods
- Added RWG Tiles for Umbrella Corp
- Increased Nut and Berry Yields for Punched Grass and Bushes
- Increased Insect Yields from Grass
- Increased Chances of Finding Wrenches in Rolling Toolboxes and Sinks
- Changed deprecated buff heatSource to Campfire Buff on Grills
- Added Chance of Propane Tank From Gas Grill Salvages
- Shifted Monkey Mansion to Tier 4
- Added Bleach and Water Purification Tablets. Bleach is loot only. Water Purification Tablets can be looted and unlocked via Medical. They require Bleach to craft. They can be used to turn Infected water into Murky Water. Credit khzmuzik for the idea.
- Fixed Some Gas Pumps Not Being Able to Upgrade
- Lowered Sounds on Augers and Chainsaws
- Removed Recyclers and Table Saws from Workbench Spawn Lists Causing Possible Nulls (Table Saws Still Spawn in POIs)
- Updated Localization for Unlocks in Living Off The Land, Advanced Engineering and Vehicle Crafting
- Added Missing Beehive Unlock to Living Off The Land Perk
- Lowered Spawn Rate of Hostile Animals
- Increased Charcoal Yields
- Added Climbable Rope Crafts. Made of Cordage and Twine. Crafts in Lots of 5
- Adjusted Prices on Workstation Parts
- Removed Unused Tags on Workstation Parts
- Moved 7 Dogs to Tier 2
- Cleaned Up Anti Nerd Poling So you HAVE to use Ladders and Ropes
- Moved Zombie Scout Runners To Gamestage 25
- Fixed Missing Food Unlocks
- Changed Iron Hoe Schematic in Farmer Class to Actual Iron Hoe
- Fixed Old Recorder Health So It Can Not Be Exploited
- Removed MRE From Munitions Box
- Added MRE to Military Vehicles and Lockers
- Sticks No Longer Scrap
- Fixed Typo in Sprains Kit in Medical Class Not Giving Schematic


- Updated the Following POIS

- rh_Fastfood_Roosters_Voltralux
- rh_GunShopKendo_Voltralux
- rh_MegaMall_VoltraLux
- rh_UmbrellaCorp_Voltralux


- Added the Following POIS
- rh_busty_beers_stripclub_SurvivorAndy
- rh_ProTech_Cafe_TheStainGaming
- rh_Roadside_Haunt_Voltralux

### Patch Notes for 9.1.1 👾

- Fixed Warning in Wellness Modlet About FoodMaxLine
- Fixed News Radio Upgrading to Wrong Radio
- Removed Working Stations and Trader from County Store
- Scrap Tools Now Require Duct Tape to Repair
- Removed Nulling Research Desks and Mechanic Benches from Randomly Spawning in POIs Causing Nulls in Quest Resets and Looting Them ( If this occurs and you do not want to wipe find the desk, destroy it with an Admin Hammer. Relogging will allow you to get back in and destroy it)
- Added Stags to Forest Spawns
- Added Dogs and Bears to Enemy Animal Spawns

### Patch Notes for 9.1.0 👾

- Updated Score to 21.1
- Updated Nursing Home for 21.1
- Updated Xyth Core for 21.1
- Removed Backpack and Duffle Bag Storage
- Removed Trash Bags from Giving a Trash Bag upon destroying it
- Moved Empty Jars to Common Ingredient Loot
- Removed Zero Percent Chance of Loot from Toaster
- Added Chance of Bowls to Common Ingredients
- Lowered Corn Seeds in Seed Pallets
- Lowered Corn Meal in Flour Bags
- Added Nitrate to Seed Pallets
- Added Special Storage Box Options to Advanced Engineering 3
- Added Text to indicate unlocks in Perks for Lawn Mower and Storage
- Slowed Down Pipe Baton Swings
- Removed Read Icons from Skill Books
- Changed Economic Values on Hobo Stew, Sham Chowder and All Pies
- Added RH Ores to Mining Action Skills
- Added a Salvage Tools Skill
- Fixed All Tags for Entity Damage and Block Damage in Action Skills
- Fixed Cash Registers having the wrong positioning
- Moved multiple items out of the Personal Workbench and into the Workbench and Table Saw
- Fixed Models on Crowbar, Katana, Telescopic Baton and Lucille
- Added Tier 5 Melee and Schematics to Loot Lists
- Rebalanced Storage Sizes
- Added Vehicle Parts to ALL new vehicles
- Replaced Gas Cans with Empty Gas Cans on ALL new vehicles
- Lowered Brick Harvest Amounts in Brick Pallets
- Added Mortar to Cobblestone Shape Recipe
- Changed recipes on Scrap Tools
- Increased Durability on Scrap Tools
- Added timers to Wellness Buffs (*Redbeard*)
- Adjusted Audio on Sprinkler
- Added Custom Font Setting on Video Menu (*Redbeard*)
- Added Mining Machine as a Miner Class Reward
- Removed Infected Water Buckets
- Clarified Irrigation Pipes and their usage
- Added Irrigation Pipes to Seed perk
- Lowered road debris slightly
- Slightly lowered rocks in wild
- Reduced amount of limestone in underground mining
- Increased all ores by 1 in surface boulders
- Added Sulphur to Concrete Accelerator recipe
- Fixed Lucky Looter Perk being over ridden by false action skill that doesn't exist
- Removed Xyth Entity Edits to Zombie AI Tasks for NPC's and Bandits overriding default game Tasks
- Added Sulphur to surface boulders
- Added Recipe for Acid made from Sulphur
- Fixed incorrect capitalization on Firearms modlet causing Linux server issues
- Added Additional Loot Roll Chances to several high loot containers like cupboards and laundry and cars
- Fixed fish recipes erroring food prep table
- Added charcoal to Mineral Water recipe
- Added Dried Chrysanthemum and Goldenrod to Mineral Water Recipe
- Added Mineral Water to WFU
- Added Exclusions for Preloader and Optimization for Dedicated Servers Causing Slowdowns (*Redbeard*)
- Removed Multiple Lines of Stat Information in Places in Action Skills Sheet
- Added Tungsten and Chrome Arrows and Bolts to Schematic in Tier 5 Loot
- Removed Unused Unlock Skill on Chrome and Tungsten Arrows and Bolts Causing Nulls
- Increased Chances of 556 and 45 Ammo in Loot
- Removed Quest Weapon Rewards from Classes
- Added mining crafting books to stiffs loot
- Fixed Model Clipping in Xl Armored Storage
- Your last craft quantity is now remembered (*Yakov*)

- Added new colors to zombie tiers so they can easily be distinguished

- Tier 1 = Green
- Tier 2 =  Blue
- Tier 3 = Yellow
- Tier 4 = Orange
- Tier 5 = Purple

- Boss/Specials = Red

- Added Tiers of Ferals and Regular zombies in spots where they were missing
- Tier 0 and 1 zombies now have normal "hands" not feral ones
- Rabbits now give 1 Hide
- Added  is_trackable="false"  tag to forge and recycler recipes to match Vanilla
- Carried over stone axes and batons stat changes from 21.1 to our weapons
- Implemented vanilla bleeding cooldowns and changes from 21.1
- Increased counts for Mining Machine
- Restructured Seed Crafting and Levels
- Restructured Food Crafting and Levels
- Fixed Being Able to Spam Wellness (*Redbeard*)
- Final Stand Quest fixed up (*Yakov*)
- Biome Exclusions and Final Stand Quest POIs widened (*Redbeard*) (*Yakov*)
- Bumped up probability of water filters in construction crates
- Increased chances of additional Screamers to Spawn
- Increased health of Screamer
- Doubled Friendly Animal Spawn Counts
- Trash Bags Now Scrap to Plastic
- Increased HP on Chain Link Fences
- Increased HP on Steel Shapes
- Steel Polish Now Used for Steel Shapes
- Added Grease Monkey Perk Tags to Recipes Missing Them
- Increased Degradation on Machine Guns
- Fixed some Weapon and Tool Mods Slots tiering 1,2,3,4,5,6 instead of 1,200,400,600,800,1000 Quality
- Increased Overall Zombie Spawning
- Reduced Blood Moon Fog
- Increased Difficulty of Blood Moons
- Reduced Storms, Rain and Fog Chances in the World
- Fixed Double Secure Chests When Destroying Placed One
- Fixed Broken Icon on Secure Chest
- Removed XP Notification
- Added a Baton Class
- Lowered Probability on Food Skill Magazines
- Updated Some POI Tags
- Fixed Flowers in Godjens Parking
- Fixed Fertilized Crop Models
- Added surprise for everyone who gets hit by a zombie on a Bicycle (*Yakov*)
- Added Limestone to Concrete Recipe
- Added Smoke and Chests to Ostrich Hotel
- Added High End Metal Weapons and T5 Weapons to Craft Skill Progression for Quality
- Remember Last Quantity Crafted Added. Remembers last crafted quantity. Ctrl-Left Click Selects 1  Shift Left Click Selects Max.  (*Yakov*)
- Cleaned up our Settings. Configs are now in 1 Config.yaml, Removed unneeded Settings (*Yakov*)
- Removed Biome Exclusion Code Causing Nulls in RWG (*Redbeard*)
- Added Quality Template 3 to Makasin Chests
- Fixed Magazine Craft Counts in Research Desk
- Updated Localization on Stove Repair Kits and Working Stoves
- Bone Fragments No Longer Scrap
- Increased Salt Loot
- Removed Meat Stews from Common Foods
- Fixed incorrect description on Skill Books
- Increased Zombie Health Across the Board
- Removed All Terrors and Mutants From Spawning For now
- Added Ores to Rock Buster Candy
- Added Lathe to Workstation Skill
- Added Gun Parts to Loot of Tier 5's
- Added Station Tools to Loot of Tier 5's
- Changed Start Date of Mod
- Added Recyclers to the World in Bench Spots. They can be destroyed to pick up and bring home
- Added Special Industrial Block with Working Escalators, Revolving Doors and Elevators in the Electrician Bench
- Added Crafting Ingredient Reductions to Master Chef Perk in Recipes where it Made Sense
- Chrome and Tungsten Arrows Unlocks Fixed (*Redbeard*)
- Refrigerators no Longer Break on Loot (*Redbeard*)
- Removed Fishing Due to Water Changes
- Bumped Zombie Bag Timers a Bit
- Fixed grammar on Tips Menu
- Fixed Icon on Smoker Schematic
- Fixed Medical Crafting Skill
- Added Mechanical Part to Mechanic Class
- Fixed Broken Vanilla POIs
- Zombie Spawning Issues Sorted May Need More Adjustments
- Added New Am News Radios that Play in Houses (*Ryper*)
- Lowered Distance and Fixed Spatial Sound on News Radios
- Remember Craft Quantity is now An Option (*Yakov*)
- Respawn for Dedis should now work properly. (*Redbeard*) (*Yakov*) (*Sphereii*)
- Fixed Capitalization on PWB Icon that may have caused Linux Icon Issue
- Fixed Apartment Floor 6 (*MMooreHead*)
- Fixed Stone Axe, Hammer and Nailgun Repair Items Missing or Mismatches. Nailgun does All, Hammer does Stone Axe and Iron, Stone Axe does Basics.
- Stoves Now Upgrade Properly
- Added New Menu Image (*Redbeard*)
- Added New Loading Screens (*Redbeard*)
- Fixed Incorrectly Named Zombie Hand Item
- Lowered Brass in Loot
- Zombie Sight Range Option Fixed (*Yakov*)
- Adjusted Zombie Spawns
- Fixed Final Stand GoToPOI Quest
- Removed Duplicate Wellness Health Entry (*Yakov*)
- Recipe Quantity Should Now Remember and Function Correctly (*Yakov*)
- Fixed Null When Sprinkler Block Class is reset in A Quest POI
- Lathe Unlock Now Fixed
- Moved Woodbury Makasin Chests
- Thinned Out Green Rad Smoke Slightly So It Appears "Thinner"
- Added Tactical Melee and Medieval Melee Weapons to Tier 5 Chests
- Final Stand Localization Updated (*Yakov*)
- Final Stand Reworked to Include Game Events, Buffs and Blood Moon events (*Yakov*)
- Increased Mayo, Butter and Milk in Refrigerator Loot
- Removed All References to Crop Control Panel in Farming Quest
- Removed Unneeded Second Microwave
- Steel and High End Bolts And Arrows Recipes Are Now Similar in Ingredients
- Removed Old Stone Wells
- Changed Craft Cobblestones for Blacksmith Forge Quest to Gather
- Added Insects and Raw Meats to Composter
- Fixed Grace Corn Fertilized Models
- Rebalanced Damage on High End Arrows and Bolts
- Added Armor Piercing to High End Arrows and Bolts
- Log Pallets Moved to Table Saw
- Added Gun Part Schematics, High End Metal Schematics to The Final Stand Rewards
- Raised XP to Level from 10000 to 14545
- Removed Feral Chances from Night Zombies at GS 1-25
- Fixed Unlock Info on Mining Tools
- Added Digger Digest to Research Desk
- Brightened Insect Icon
- Added Crafts to Food Quest to Prevent Quest Clipping
- First Aid Kit Storage Now Returns to You When Destroyed
- Recycler Model Fixes
- Increased Corn Meal Use in Recipes
- Added Propane Tank Requirement to Recycler
- Added Caliper requirement to Ammo Table
- Fixed Marksman not Accepting AP and HP Ammo
- Increased Damage on Tier 5 Rifles
- Removed Unneeded Shotgun Ammo
- You Now Receive 1 Action Skill Book and 2 Craft Books in Each Class Quest Step You Complete
- Melee Classes Now Receive a Special Weapon at the End of their Quest Line
- Starting Class Weapons Given now Level 400
- Added 3 Waters to Chef Class
- Fixed Mesh Inconsistencies on Research Desk
- Added Action Skill Dev Book 
- Renamed All Of our Admin Books to Dev
- Added All Crafting Skills for RH to Dev Crafting Book
- Last Stand Cleanup and Fixes (*Yakov*)
- Fixed Water Stat Resetting After Too Many Deaths (*Yakov*)
- Fixed Localization on Electric Wire Relays

### Added the following POIS:
- rh_Mo_Power_Electronics_A19_DeadWalker
- rh_Paper_Mill_A19_DeadWalker
- rh_Crack_a_Book_HQ_A19_DeadWalker
- rh_casablanca_Morfitall
- rh_MegaMall_VoltraLux (Probably will not spawn naturally but there is a chance)
- rh_community_centre_VoltraLux
- rh_7Dogs_A21_0_by_KillerBee
- rh_Bunker_Camo_A21_0_by_KillerBee
- rh_Burnt_Bunker_A21_0_by_KillerBee
- rh_Parking_A21_0_by_KillerBee
- rh_Playground_A21_0_by_KillerBee
- rh_SmallFitness_A21_0_by_KillerBee
- rh_SurvivorShelter_A21_0_by_KillerBee
- rh_sap_ultrascans_clinic_SurvivorAndy
- rh_sap_woodbury_SurvivorAndy
- rh_sap_tesko_supermarket_SurvivorAndy
- rh_sap_malibu_club_SurvivorAndy
- rh_sap_airstrip_SurvivorAndy
- rh_junkyard_xtiericee
- rh_Crime_At_The_Manor_TheStainGaming
- rh_CountryTown_Plaza_TheStainGaming
- rh_elbarrio_Morfitall
- rh_UmbrellaCorp_Voltralux
- rh_KickFlipz_Voltralux
- rh_GunShopKendo_Voltralux
- rh_CentralStation_Voltralux
- rh_Fastfood_Roosters_Voltralux
- rh_EternalRests_TheStainGaming
- rh_7_Days_to_Fries_VoltraLux
- rh_City_Square_TheStainGaming

